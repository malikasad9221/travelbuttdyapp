package com.example.travelbuddy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

    EditText mFullName,mEmail,mPassowrd,mPhone;

    TextView mLoginBtn;
    FirebaseAuth fAuth;
    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFullName= findViewById(R.id.txtFullName);
        mEmail= findViewById(R.id.txtEmail);
        mPassowrd= findViewById(R.id.txtPassword);
        mPhone= findViewById(R.id.txtPhone);
        Button mRegisterButton= (Button)findViewById(R.id.btnRegister);
        mLoginBtn= findViewById(R.id.txtLoginHere);

        fAuth= FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);



        if (fAuth.getCurrentUser()!= null)
        {
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            }
        });


    }

    public void RegisterButtonClick(View view) {

        String email= mEmail.getText().toString().trim();
        String password = mPassowrd.getText().toString().trim();
        String fullName= mFullName.getText().toString().trim();
        String phone= mPhone.getText().toString().trim();
        if (TextUtils.isEmpty(email)){
            mEmail.setError("Email is Required!");
            return;
        }
        if (TextUtils.isEmpty(password)){
            mPassowrd.setError("Email is Required!");
            return;
        }

        if (password.length()<6){
            mPassowrd.setError("Password is TooShort!");
            return;
        }
        if (TextUtils.isEmpty(fullName)){
            mFullName.setError("Full Name is Required!");
            return;
        }
        if (TextUtils.isEmpty(phone)){
            mPhone.setError("Phone Number is Required!");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful())
                {
                    Toast.makeText(RegisterActivity.this,"User Created",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                }
                else
                {
                    Toast.makeText(RegisterActivity.this,"Error!"+task.getException().getMessage(),Toast.LENGTH_LONG).show();

                }
        }

        });

    }
}
