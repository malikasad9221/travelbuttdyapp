package com.example.travelbuddy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    EditText mEmail,mPassword;
    Button mLoginButton;
    ProgressBar progressBar;
    FirebaseAuth fAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mEmail= findViewById(R.id.txtEmail);
        mPassword= findViewById(R.id.txtPassword);
        fAuth= FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.progressBar);
        mLoginButton=(Button)findViewById(R.id.btnLogin);


        TextView mRegisterButton = (TextView)findViewById(R.id.textView2);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),RegisterActivity.class));
            }
        });

        TextView mForgotPassword = (TextView)findViewById(R.id.txtForgot);
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ForgetPasswordActivity.class));
            }
        });

    }

    public void btnLoginClick(View view) {

        String email= mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)){
            mEmail.setError("Email is Required!");
            return;
        }
        if (TextUtils.isEmpty(password)){
            mPassword.setError("Email is Required!");
            return;
        }



        progressBar.setVisibility(View.VISIBLE);

        //Authenticate the User:
        fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    Toast.makeText(LoginActivity.this,"Login Successful",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
                else {
                    Toast.makeText(LoginActivity.this,"Error!"+task.getException().getMessage(),Toast.LENGTH_LONG).show();

                }

            }
        });

        startActivity(new Intent(getApplicationContext(),MainActivity.class));

    }
}
